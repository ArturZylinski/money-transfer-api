import javax.inject._

import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import play.api.{Configuration, Environment}
import v1.transfer._

/**
  * Sets up custom components for Play.
  *
  * https://www.playframework.com/documentation/latest/ScalaDependencyInjection
  */
class Module(environment: Environment, configuration: Configuration)
  extends AbstractModule
    with ScalaModule {

  override def configure() = {
        bind[AccountRepository].to[AccountRepositoryImpl].in[Singleton]
//    bind(classOf[AccountRepository]).to(classOf[AccountRepositoryImpl]).asEagerSingleton()

//    bind[TransactionRepository].to[TransactionRepositoryImpl].in[Singleton]
  }
}
