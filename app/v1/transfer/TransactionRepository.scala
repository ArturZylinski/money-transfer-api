package v1.transfer

import java.util.UUID

import javax.inject.{Inject, Singleton}

import akka.actor.ActorSystem
import play.api.libs.concurrent.CustomExecutionContext
import play.api.MarkerContext

import scala.concurrent.Future


final case class Transaction(fromAccountId: UUID, toAccountId: UUID, value: Long, status: String, createdAt: Long, updatedAt: Long)


class TransactionExecutionContext @Inject()(actorSystem: ActorSystem) extends CustomExecutionContext(actorSystem, "repository.dispatcher")


/**
  * A pure non-blocking interface for the TransactionRepository.
  */
trait TransactionRepository {
  def init(fromAccountId: UUID, toAccountId: UUID, value: Long)(implicit mc: MarkerContext): Future[Boolean]

  def get(id: UUID)(implicit mc: MarkerContext): Future[Option[Transaction]]

  // list incoming and outgoing transactions
  def list(accountId: UUID)(implicit mc: MarkerContext): Future[Iterable[Transaction]]

  // in the real life, status change should be handeled with State Machine
  def success(id: UUID)(implicit mc: MarkerContext): Future[Boolean]
  def fail(id: UUID)(implicit mc: MarkerContext): Future[Boolean]
}
