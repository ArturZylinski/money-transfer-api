package v1.transfer

import java.util.UUID
import javax.inject._
import play.api._
import play.api.data.validation.ValidationError
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}


case class MoneyTransferResource(fromAccountId: UUID, toAccountId: UUID, value: Long)

object MoneyTransferResource {
  // TODO:
  //  val positiveInteger: Reads[Long] =
  //    Reads.StringReads.filter(ValidationError("Invalid value given!"))(v => {
  //      v > 0
  //    })

  implicit val transferReads: Reads[MoneyTransferResource] = (
    (JsPath \ "fromAccountId").read[UUID] and
    (JsPath \ "toAccountId").read[UUID] and
    (JsPath \ "value").read[Long] // (positiveInteger)
  ) (MoneyTransferResource.apply _)
}


@Singleton
class MoneyTransferController @Inject()(
    accountRepository: AccountRepository)(cc: ControllerComponents) extends AbstractController(cc) {

  def process() = Action(parse.json) { request =>
    val transferResult = request.body.validate[MoneyTransferResource]
    transferResult.fold(
      errors => {
        BadRequest(Json.obj("message" -> JsError.toJson(errors)))
      },
      transfer => {
        accountRepository.transfer(transfer.fromAccountId, transfer.toAccountId, transfer.value)
        Created("")
      }
    )
  }
}
