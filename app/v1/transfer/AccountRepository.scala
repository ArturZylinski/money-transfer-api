package v1.transfer

import java.util.UUID

import javax.inject.{Inject, Singleton}

import akka.actor.ActorSystem
import play.api.libs.concurrent.CustomExecutionContext
import play.api.MarkerContext

import scala.concurrent.Future


final case class Account(id: UUID, var balance: Long)


class AccountExecutionContext @Inject()(actorSystem: ActorSystem) extends CustomExecutionContext(actorSystem, "repository.dispatcher")


/**
  * A pure non-blocking interface for the AccountRepository.
  */
trait AccountRepository {
  def transfer(fromId: UUID, toId: UUID, value: Long)(implicit mc: MarkerContext): Boolean
}


/**
  * A trivial implementation for the Account Repository.
  *
  * A custom execution context is used here to establish that blocking operations should be
  * executed in a different thread than Play's ExecutionContext, which is used for CPU bound tasks
  * such as rendering.
  */
@Singleton
class AccountRepositoryImpl @Inject()()(implicit ec: AccountExecutionContext) extends AccountRepository {

  // simulate database
  private var accounts = collection.mutable.Map(List(
    Account(UUID.fromString("5100a5b4-4853-499d-8844-58f28ede47a7"), 100),
    Account(UUID.fromString("7fbcaa11-20a4-4649-91bc-c510441af686"), 0),
    Account(UUID.fromString("a5791991-a944-41d5-9bde-538f7a5e2873"), -20),
    Account(UUID.fromString("a3325120-ef2b-42ea-88bc-f88ffa464372"), 100000000),
    Account(UUID.fromString("2afec675-15c6-4260-80c1-6d52e30fbe3e"), 20000),
    Account(UUID.fromString("1989be50-f0c4-4930-b2f4-39253bbd8acb"), 100000),
    Account(UUID.fromString("90b3cd2f-92a6-425a-935e-999485097fb1"), 675439532),
    Account(UUID.fromString("ec38fe4e-4ca7-484d-8409-bc69387aac7d"), 57834),
    Account(UUID.fromString("0dd4ab25-e929-469a-b356-dcef8a26cfb3"), 67543),
    Account(UUID.fromString("d2e0f21e-cd97-46a7-b6e2-47382b601e5e"), 5476)
  ).map(acc => (acc.id.toString(), acc)): _*)

  override def transfer(fromId: UUID, toId: UUID, value: Long)(implicit mc: MarkerContext): Boolean = {
    if (!(accounts contains fromId.toString())) {
      throw new AccountNotFoundException("Account not found, id: " + fromId.toString())
    }
    if (!(accounts contains toId.toString())) {
      throw new AccountNotFoundException("Account not found, id: " + toId.toString())
    }

    var fromAccount = (accounts get fromId.toString()).get

    // check against "non-sufficient funds" (NSF)
    if (fromAccount.balance < value) {
      val msg = "Not enough funds on account: " + fromAccount.id + ", balance: " + fromAccount.balance + ", requested amount: " + value.toString()
      throw new NonSufficientFundsException(msg)
    }

    fromAccount.balance = fromAccount.balance - value

    var toAccount = (accounts get toId.toString()).get

    toAccount.balance = toAccount.balance + value

    accounts(fromId.toString()) = fromAccount
    accounts(toId.toString()) = toAccount

    for ((k,v) <- accounts) printf("key: %s, value: %s\n", k, v.balance)

    return true
  }

}


/**
  * Custom exceptions
  */

// TOOD: returns 409 status code
// abstract case class DataConflictException(msg: String) extends Exception(msg) {}

final case class AccountNotFoundException(msg: String) extends Exception(msg) {}
final case class NonSufficientFundsException(msg: String) extends Exception(msg) {}
