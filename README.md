# Money Transfer API

## Build a binary version

In the Play console (`sbt`), simply type dist:

```bash
dist
```

Read more: https://www.playframework.com/documentation/2.6.x/Deploying#Using-the-dist-task

follow standards:
https://pending.schema.org/MoneyTransfer

RAML

Assumptions:
- from & to account use the same currency
- auth is done on the higher level (JWT via Kong, "I want to send X money to Y account") - that's being translated to: MoneyTransfer value X from A account to B
- supposted to happen now (some system handles async, queues, retries, ...)
- internal service
- anomally detection


Validation:
- mimetype, send size, ...
- check the json request vs schema
  - currency: ISO_4217
  - accountId: guid
- business logic:
  - from != to account

  409:
  - does both account exist
  - does both account has the same currency
  - does "from" account has enough money

405: Method Not Allowed
406: Not Acceptable
408: Request Timeout


Return:
 - transaction id
 - 

Random notes:
- store value in MICROS (because of some currencies)
- describe cons and possible full-solution (kafka, dedup, locks, ...)
- from/to - internal ids (mapped already from iban/bic) - MUST HAVE for security reasons - the service above must do the mapping
- !! do not accept compressed data - the archive win is not worth !!
- 404 for all other routes
- account could be blocked


Sources:
 - https://en.wikipedia.org/wiki/ISO_4217
 - https://pending.schema.org/MoneyTransfer
 - Standards:
  - Stripe
  - Monzo: https://docs.monzo.com/#transactions
  - N26: http://pierrickpaul.fr/n26/global.html#transaction

 - http://web.cs.ucla.edu/classes/winter16/cs111/scribe/9c/index.html
 - https://monzo.com/blog/2018/04/05/how-monzo-to-monzo-payments-work/ 
 - https://lightningsecurity.io/blog/race-conditions/
