import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test._
import play.api.test.Helpers._
import play.api.test.CSRFTokenHelper._


class MoneyTransferControllerSpec extends PlaySpec with GuiceOneAppPerTest {

  "Routes" should {

    "send 404 on a bad request" in  {
      route(app, FakeRequest(GET, "/")).map(status(_)) mustBe Some(NOT_FOUND)
    }

    "send 415 on empty headers" in {
      route(app, FakeRequest(POST, "/v1/transfers")).map(status(_)) mustBe Some(UNSUPPORTED_MEDIA_TYPE)
    }

    "send 201 on a good request" in {
      val request = FakeRequest(POST, "/v1/transfers").withHeaders(HOST -> "localhost:9000").withBody()
      val response = route(app, request).get

      status(response) mustBe Status.Created
      contentType(response) mustBe Some("application/json")
    }
  }

  "MoneyTransferController" should {
  }

}